.. _scope:

Scope of This Document
********************************************************************************************************************************

The purpose of this document is to provide an overview of the
recommended procedures and the available resources for Debian developers
and maintainers.

The procedures discussed within include how to become a member
(:ref:`new-maintainer`); how to create new packages
(:ref:`newpackage`) and how to upload packages (:ref:`upload`);
how to handle bug reports (:ref:`bug-handling`); how to move,
remove, or orphan packages (:ref:`archive-manip`); how to port
packages (:ref:`porting`); and how and when to do interim releases
of other maintainers' packages (:ref:`nmu`).

The resources discussed in this reference include the mailing lists
(:ref:`mailing-lists`) and servers (:ref:`server-machines`); a
discussion of the structure of the Debian archive (:ref:`archive`);
explanation of the different servers which accept package uploads
(:ref:`upload-ftp-master`); and a discussion of resources which can
help maintainers with the quality of their packages (:ref:`tools`).

It should be clear that this reference does not discuss the technical
details of Debian packages nor how to generate them. Nor does this
reference detail the standards to which Debian software must comply. All
of such information can be found in the `Debian Policy
Manual <https://www.debian.org/doc/debian-policy/>`__.

Furthermore, this document is *not an expression of formal policy*. It
contains documentation for the Debian system and generally agreed-upon
best practices. Thus, it is not what is called a *normative* document.

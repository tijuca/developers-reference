Source: developers-reference
Section: doc
Priority: optional
Maintainer: Developers Reference Maintainers <debian-policy@lists.debian.org>
Uploaders: Hideki Yamane <henrich@debian.org>,
 Holger Levsen <holger@debian.org>,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends-Indep: latexmk,
 python3-distro-info,
 python3-sphinx,
 python3-stemmer,
 tex-gyre,
 texinfo,
 texlive-fonts-recommended,
 texlive-lang-cyrillic,
 texlive-lang-french,
 texlive-lang-german,
 texlive-lang-italian,
 texlive-lang-japanese,
 texlive-latex-extra,
 texlive-latex-recommended,
Build-Depends: debhelper-compat (= 13)
Vcs-Git: https://salsa.debian.org/debian/developers-reference.git
Vcs-Browser: https://salsa.debian.org/debian/developers-reference
Homepage: https://www.debian.org/doc/devel-manuals#devref

Package: developers-reference
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 sensible-utils,
 ${sphinxdoc:Depends},
Suggests: doc-base, firefox-esr | www-browser
Description: guidelines and information for Debian developers
 This package contains the Debian Developer's Reference, a set of
 guidelines and best practices which has been established by and for
 the community of Debian developers and maintainers. If you are not
 maintaining Debian packages, you probably do not need this package.
 .
 Table of Contents:
 .
 ${TOC:en}
 .
 This package contains the English version of the Developer's
 Reference.  The French, German, Italian, Russian and Japanese translations are
 available in developers-reference-fr, developers-reference-de,
 developers-reference-it, developers-reference-ru and developers-reference-ja.

Package: developers-reference-de
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 developers-reference,
 ${sphinxdoc:Depends},
Suggests: doc-base
Description: guidelines and information for Debian developers, in German
 This package contains the German translation of Debian Developer's
 Reference (package: developers-reference), a set of guidelines and
 best practices which has been established by and for the community of
 Debian developers and maintainers. If you are not maintaining Debian
 packages, you probably do not need this package.
 .
 Table of Contents (in English):
 .
 ${TOC:en}

Package: developers-reference-fr
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 developers-reference,
 ${sphinxdoc:Depends},
Suggests: doc-base
Description: guidelines and information for Debian developers, in French
 This package contains the French translation of Debian Developer's
 Reference (package: developers-reference), a set of guidelines and
 best practices which has been established by and for the community of
 Debian developers and maintainers. If you are not maintaining Debian
 packages, you probably do not need this package.
 .
 Table of Contents (in English):
 .
 ${TOC:en}

Package: developers-reference-ja
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 developers-reference,
 ${sphinxdoc:Depends},
Suggests: doc-base
Description: guidelines and information for Debian developers, in Japanese
 This package contains the Japanese translation of Debian Developer's
 Reference (package: developers-reference), a set of guidelines and
 best practices which has been established by and for the community of
 Debian developers and maintainers. If you are not maintaining Debian
 packages, you probably do not need this package.
 .
 Table of Contents (in English):
 .
 ${TOC:en}

Package: developers-reference-ru
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 developers-reference,
 ${sphinxdoc:Depends},
Suggests: doc-base
Description: guidelines and information for Debian developers, in Russian
 This package contains the Russian translation of Debian Developer's
 Reference (package: developers-reference), a set of guidelines and
 best practices which has been established by and for the community of
 Debian developers and maintainers. If you are not maintaining Debian
 packages, you probably do not need this package.
 .
 Table of Contents (in English):
 .
 ${TOC:en}

Package: developers-reference-it
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debian-policy,
 developers-reference,
 ${sphinxdoc:Depends},
Suggests: doc-base
Description: guidelines and information for Debian developers, in Italian
 This package contains the Italian translation of Debian Developer's
 Reference (package: developers-reference), a set of guidelines and
 best practices which has been established by and for the community of
 Debian developers and maintainers. If you are not maintaining Debian
 packages, you probably do not need this package.
 .
 Table of Contents (in English):
 .
 ${TOC:en}
